#!/usr/bin/env bash

./check.sh

for node in k3s{1..16} backup{1,2} build2 redtail{,2} whitestorm2 jellyfin;
do
  echo "Garbage collecting ${node}"
  ssh -t $node 'sudo nix-collect-garbage -d'
done

for node in k3s{1..16};
do
  echo "Pruning ${node}"
  ssh -t $node 'sudo k3s crictl rmi --prune'
done

sudo -E -s ./deploy-all.sh

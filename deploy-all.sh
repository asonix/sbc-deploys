#!/usr/bin/env bash

for node in k3s{1..16} backup{1,2} build2 redtail{1,2} whitestorm2 jellyfin;
do
  echo "Deplying to ${node}"
  ./deploy.sh .#$node
done

{ config, ... }:

{
  services.openssh.settings.PasswordAuthentication = false;

  sops = {
    age.keyFile = "/home/asonix/.config/sops/age/keys.txt";
    age.generateKey = true;

    secrets.builderKey = {
      format = "binary";
      sopsFile = ../../secrets/builderKey.bin;
    };
  };

  nix.buildMachines = [{
    hostName = "192.168.5.10";
    systems = [ "aarch64-linux" "x86_64-linux" ];
    protocol = "ssh";
    sshUser = "builder";
    sshKey = "${config.sops.secrets.builderKey.path}";
    maxJobs = 12;
    speedFactor = 2;
    supportedFeatures = [ "nixos-test" "benchmark" "big-parallel" "kvm" ];
    mandatoryFeatures = [ ];
  }];
  nix.distributedBuilds = true;

  nix.extraOptions = ''
    builders-use-substitutes = false
  '';
}

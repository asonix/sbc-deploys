{ device, subvolumes ? [ ] }:

{ lib, ... }:

{
  fileSystems = (builtins.foldl'
    (acc: subvol:
      let
        length = builtins.stringLength subvol;
        dirName = builtins.substring 1 length subvol;
      in
      acc // {
        "/storage/${dirName}" = {
          inherit device;
          fsType = "btrfs";
          options = [ "defaults" "compress=zstd" "rw" "subvol=${subvol}" ];
        };
      })
    { }
    subvolumes);
}

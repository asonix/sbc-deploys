{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [ bottom cryptsetup git htop screen ];

  time.timeZone = "UTC";

  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;
  };

  zramSwap = {
    enable = true;
    memoryPercent = 75;
  };
}

{
  server = { enable ? true }:
    { config, pkgs, ... }: {
      networking.firewall.enable = false;

      environment.systemPackages = with pkgs; [
        k3s
        (writeShellScriptBin "kube" (builtins.readFile ./kubectl))
      ];

      sops.secrets.k3s_env = {
        format = "binary";
        sopsFile = ../../secrets/k3s_env.bin;
      };

      services.k3s = {
        inherit enable;
        environmentFile = config.sops.secrets.k3s_env.path;
        extraFlags = "--disable traefik --disable servicelb --cluster-cidr=10.42.0.0/16,2001:cafe:42::/56 --service-cidr=10.43.0.0/16,2001:cafe:43::/112";
        role = "server";
      };
    };

  agent = { serverIp, enable ? true }:
    { config, ... }: {
      networking.firewall.enable = false;

      sops.secrets.k3s_token = {
        format = "yaml";
        sopsFile = ../../secrets/k3s.yaml;
      };

      services.k3s = {
        inherit enable;
        role = "agent";
        serverAddr = "https://${serverIp}:6443";
        tokenFile = config.sops.secrets.k3s_token.path;
      };
    };
}

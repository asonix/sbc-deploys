{ instances ? [ ], localMountDir ? null, enabled ? true }:

{ config, pkgs, ... }:

let
  btrbkPrimary = { subvolumes ? [ ] }: {
    snapshot_dir = "@snapshots";
    subvolume =
      builtins.foldl' (acc: subvol: acc // { ${subvol} = { }; }) { } subvolumes;
  };

  btrbkSecondary = { targetDir, subvolumes ? [ ] }: {
    target = "send-receive ${targetDir}";
    subvolume = builtins.foldl'
      (acc: subvol:
        acc // {
          ${subvol} = {
            snapshot_dir = "@snapshots";
            snapshot_preserve_min = "all";
            snapshot_create = "no";
          };
        })
      { }
      subvolumes;
  };

  primary = mountDir: subvolumes: {
    snapshot_preserve_min = "8h";
    snapshot_preserve = "24h 2d 1w";
    transaction_log = "/var/log/btrbk.log";
    volume = { "${mountDir}" = btrbkPrimary { inherit subvolumes; }; };
  };

  secondary = primaryIp: mountDir: subvolumes: {
    backend_remote = "btrfs-progs-sudo";
    ssh_identity = config.sops.secrets.private_key.path;
    ssh_user = "btrbk";
    stream_buffer = "512m";
    target_preserve = "24h 7d";
    target_preserve_min = "24h";
    transaction_log = "/var/log/btrbk.log";
    volume = {
      "ssh://${primaryIp}${mountDir}" = btrbkSecondary {
        targetDir = "${mountDir}/@snapshots";
        inherit subvolumes;
      };
    };
  };

  backup = instances: {
    backend_remote = "btrfs-progs-sudo";
    ssh_identity = config.sops.secrets.private_key.path;
    ssh_user = "btrbk";
    # stream_buffer = "512m";
    target_preserve = " 2d 10w 6m";
    target_preserve_min = "1d";
    transaction_log = "/var/log/btrbk.log";
    volume = builtins.foldl'
      (acc:
        { primaryIp, mountDir, name, subvolumes }:
        acc // {
          "ssh://${primaryIp}${mountDir}" = btrbkSecondary {
            targetDir = "${localMountDir}/@snapshots/${name}";
            inherit subvolumes;
          };
        })
      { }
      instances;
  };
in
{
  sops.secrets.private_key = {
    format = "yaml";
    sopsFile = ../../secrets/btrbk.yaml;
    owner = config.users.users.btrbk.name;
    group = config.users.users.btrbk.group;
  };

  environment.systemPackages = with pkgs;
    [
      btrbk
      btrfs-progs
      (writeShellScriptBin "restore-snapshot"
        (builtins.readFile ./restore-snapshot))
      (writeShellScriptBin "restore-all-snapshots"
        (builtins.readFile ./restore-all-snapshots))
    ] ++ (if localMountDir != null then
      [
        (writeShellScriptBin "make-backup-subdirectories" (builtins.foldl'
          (acc:
            { name, ... }: ''
              ${acc}
              mkdir -p ${localMountDir}/@snapshots/${name}'') ""
          instances))
      ]
    else
      [ ]);

  services.btrbk = {
    sshAccess = [{
      key =
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHTqU3EvTgY5/e9m6YyQWypQPK58t9iPmPnPYAvnODGB asonix@lionheart";
      roles = [ "source" "info" "send" ];
    }];
    instances =
      if localMountDir == null then
        (builtins.foldl'
          (acc:
            { primaryIp ? null, mountDir, subvolumes, name ? "btrbk" }:
            let
              selected =
                if primaryIp == null then
                  (primary mountDir subvolumes)
                else
                  (secondary primaryIp mountDir subvolumes);
            in
            acc // {
              ${name} = {
                onCalendar = "hourly";
                settings = selected;
              };
            })
          { }
          instances)
      else if enabled then {
        btrbk = {
          onCalendar = "*-*-* 0/4:00:00";
          settings = (backup instances);
        };
      } else { };
  };
}

{ macAddress ? null, selfIp, selfIp6 }:

{
  networking = {
    interfaces.end0 = {
      inherit macAddress;
      ipv4.addresses = [{
        address = selfIp;
        prefixLength = 24;
      }];
      ipv6.addresses = [{
        address = selfIp6;
        prefixLength = 64;
      }];
    };
    defaultGateway = "192.168.20.1";
    defaultGateway6 = {
      address = "2001:db8:0:20::1";
      interface = "end0";
    };
    nameservers = [
      "192.168.20.1"
      "2001:db8:0:20::1"
    ];
  };
}

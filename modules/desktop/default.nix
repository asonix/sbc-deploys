{ pkgs, lib, ... }:

{
  users.users.asonix = {
    extraGroups = [ "networkmanager" ];
    shell = pkgs.zsh;
  };
  networking.networkmanager.enable = true;
  i18n.defaultLocale = "en_US.utf8";
  services = {
    automatic-timezoned.enable = true;
    geoclue2.enableDemoAgent = lib.mkForce true;
    xserver = {
      xkb.layout = "us";
      xkb.variant = "";
      enable = true;
      desktopManager = {
        gnome.enable = true;
      };
      displayManager.gdm.enable = true;
    };
    flatpak.enable = true;
    printing.enable = true;
    avahi = {
      enable = true;
      openFirewall = true;
    };
    pipewire = {
      enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
      jack.enable = true;
    };
  };
  programs = {
    dconf.enable = true;
    zsh.enable = true;
  };
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;

  environment.systemPackages = with pkgs; [
    android-tools
    bottom
    cachix
    firefox
    git
    htop
    screen
    vim
    zsh

    gnomeExtensions.arc-menu
    gnomeExtensions.dash-to-dock
    gnomeExtensions.dash-to-panel
    gnomeExtensions.gjs-osk
    gnomeExtensions.one-window-wonderland

    usbutils
    pciutils
    wireguard-tools
  ];

  environment.sessionVariables = {
    MOZ_ENABLE_WAYLAND = "1";
  };

  networking.hostName = "pinetab2";
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
}

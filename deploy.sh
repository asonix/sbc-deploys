#!/usr/bin/env bash

export LOCAL_KEY=/etc/nix/cache-priv-key.pem
sudo -E -s nix run github:serokell/deploy-rs -- --skip-checks "${@:1}"
